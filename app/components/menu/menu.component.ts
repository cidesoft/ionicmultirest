import { Component, OnInit } from "@angular/core";
import { ServiceProvider } from "../../../providers/service";
import { Componente } from "../../interfaces/interfaces";
import { Observable } from "rxjs";
import { TranslateService } from "@ngx-translate/core"; // add this
import { Events, MenuController } from "@ionic/angular";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { Values } from "src/providers/values";
import { UsersProvider } from "src/providers/users";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"]
})
export class MenuComponent implements OnInit {
  componentes: Observable<Componente[]>;
  user: any;

  constructor(
    private dataService: ServiceProvider,
    private translate: TranslateService,
    public events: Events,
    private router: Router,
    private storage: Storage,
    public menuCtrl: MenuController,
    public values: Values,
    public usersProv: UsersProvider
  ) {
    let userLang = navigator.language.split("-")[0];
    userLang = /(english|deutsch)/gi.test(userLang) ? userLang : "english";
    this.translate.use(userLang);

    this.events.subscribe("user: change", user => {
      if (user || user != null) {
        console.log("userchange");
        console.log(user);
        this.user = user;

        this.values.isLoggedIn = true;

        this.router.navigateByUrl("home");
        this.menuCtrl.enable(true);
      } else {
        this.router.navigateByUrl("login");
        this.menuCtrl.enable(false);
      }
    });

    this.storage.ready().then(() => {
      this.storage.get("user").then(val => {
        console.log(val);
        if (val != null) {
          this.user = val;
          this.router.navigateByUrl("home");
          this.menuCtrl.enable(true);
        } else {
          this.router.navigateByUrl("login");
          this.menuCtrl.enable(false);
        }
      });
    });
  }

  ngOnInit() {
    this.componentes = this.dataService.getMenuOptions();
  }

  logout() {
    this.usersProv.logoutUser().then(() => {
      this.storage.remove("user");
      this.user = null;
      this.storage.remove("cart_list");
      this.router.navigateByUrl("/login");
      this.menuCtrl.enable(false);
    });
  }
}
